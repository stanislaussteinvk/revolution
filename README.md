# revolution

The package can either be installed with or without automatic loading of
the vignette.

If the vignette is not loaded, the package needs to import the RKI data.
This can either be done via the automatic update_rki_data() function, or 
via the manual_load_rki_data() function, if a local RKI file is present. 
Please refere to their respective documentations.

If the vignette is loaded, the installation takes some time, as the data
will be downloaded automatically. 

If the user uses Linux this automatic download often fails. In this case
the user should install the package without loading the vignette. Afterwards 
he can install the data via update_rki_data(method="wget"). The vignette can
now be loaded. A manual import via manual_load_rki_data() is also possible.
